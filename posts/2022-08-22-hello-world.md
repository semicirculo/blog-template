---
title: Welcome to my blog
tags:  hello world, example
---

This is a sample article written in Markdown[^1] and here is a program that prints a message:

```python
# This is a sample of Python code
my_string = "Hello world!"
print(my_string)
```

This is a random inlined *TeX math* formula $e^{i\pi} + 1 = 0$ and this is a matrix:

$$X = \begin{bmatrix}\\
x_{1 1} & x_{1 2}\\
x_{2 1} & x_{2 2}
\end{bmatrix}$$

To learn more about this flavor of Markdown visit: <https://pandoc.org/MANUAL.html#pandocs-markdown>

[^1]: [Markdown](https://en.wikipedia.org/wiki/Markdown) is a lightweight markup language for creating formatted text using a plain-text editor.

